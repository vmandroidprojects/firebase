package com.example.drodriguez.firebase;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

/**
 * Vista de Login donde el usuario inica sesión con su correo electrónico y contraseña
 */
public class LoginActivity extends AppCompatActivity{
    private EditText email;

    private EditText password;

    private Button loginButton;
    private Button signInButton;
    private Button passwordRecovery;
    //Firebase
    // Crear usuario y autenticarse.
    private FirebaseAuth mAuth;
    //Listener del estado de la Autenticación.
    private FirebaseAuth.AuthStateListener mAuthListener;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mAuth = FirebaseAuth.getInstance();

        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);

        loginButton = (Button) findViewById(R.id.login_button);
        signInButton = (Button) findViewById(R.id.signIn_button);
        passwordRecovery = (Button) findViewById(R.id.passwordRecovery);

        mAuthListener = new FirebaseAuth.AuthStateListener(){
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth){
                if(firebaseAuth.getCurrentUser() != null){

                    startActivity(new Intent(LoginActivity.this,AccountActivity.class));

                }
            }
        };

        //Acción al presionar el boton de Login.
        loginButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                startSignIn(email.getText().toString(),password.getText().toString());
            }
        });

        //Acción al presionar el boton de Registo.
        signInButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                startActivity(new Intent(LoginActivity.this,RegisterActivity.class));
            }
        });

        passwordRecovery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(email.getText())) {
                    Toast.makeText(LoginActivity.this, "Favor de escribir tu correo electrónico", Toast.LENGTH_LONG).show();
                } else {
                    mAuth.sendPasswordResetEmail(String.valueOf(email)).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Toast.makeText(LoginActivity.this, "Correo de Confirmación enviado", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    //Verifica que se pueda Iniciar sesión correctamente.
    private void startSignIn(String email,String password){
        if(TextUtils.isEmpty(email) || TextUtils.isEmpty(password)){
            Toast.makeText(LoginActivity.this,"No puedes dejar campos vacios", Toast.LENGTH_LONG).show();
        }else {
            mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (!task.isSuccessful()) {
                        String error = task.getException().getClass().toString();
                        if (error.equals("class com.google.firebase.auth.FirebaseAuthInvalidCredentialsException")) {
                            Toast.makeText(LoginActivity.this, "correo no valido", Toast.LENGTH_LONG).show();
                        }else if(error.equals("class com.google.firebase.auth.FirebaseAuthInvalidUserException")){
                            Toast.makeText(LoginActivity.this, "el usuario no existe", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });
        }
    }
}