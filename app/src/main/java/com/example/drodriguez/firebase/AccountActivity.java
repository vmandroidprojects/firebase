package com.example.drodriguez.firebase;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class AccountActivity extends AppCompatActivity {
    private TextView name;
    private TextView email;
    private TextView estatus;
    private TextView bolsa;
    private TextView caja;

    private Button signOutButton;

    private ListView activitiesList;
    private ArrayList<String> activitiesArray = new ArrayList<>();

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    String uId = FirebaseAuth.getInstance().getCurrentUser().getUid();
    DatabaseReference myRef = database.getReference("tpvVM").child("usuarios").child(uId);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);

        name = (TextView) findViewById(R.id.nombre);
        email = (TextView) findViewById(R.id.email);
        estatus = (TextView) findViewById(R.id.estatus);
        bolsa = (TextView) findViewById(R.id.bolsa);
        caja = (TextView) findViewById(R.id.caja);
        signOutButton = (Button) findViewById(R.id.signOutButton);

        activitiesList = (ListView) findViewById(R.id.activitiesList);
        activitiesArray.add("EDITAR PERFIL");
        activitiesArray.add("VENTAS");
        activitiesArray.add("MOVIMIENTOS");
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,activitiesArray);

        activitiesList.setAdapter(arrayAdapter);

        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener(){
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth){
                if(firebaseAuth.getCurrentUser() == null){
                    startActivity(new Intent(AccountActivity.this,LoginActivity.class));
                }
            }
        };

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                name.setText(dataSnapshot.child("nombre").getValue(String.class)+" "+dataSnapshot.child("apellido").getValue(String.class));
                email.setText(dataSnapshot.child("correo").getValue(String.class));
                estatus.setText(dataSnapshot.child("estatus").getValue(String.class));
                bolsa.setText("$"+dataSnapshot.child("bolsa").getValue(String.class));
                caja.setText("$"+dataSnapshot.child("caja").getValue(String.class));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        signOutButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                FirebaseAuth.getInstance().signOut();
            }
        });

        activitiesList.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch(position){
                    case 0:
                        startActivity(new Intent(AccountActivity.this,EditActivity.class));
                        break;
                    case 1:
                        startActivity(new Intent(AccountActivity.this,VentasActivity.class));
                        break;
                    case 2:
                        startActivity(new Intent(AccountActivity.this,MovimientosActivity.class));
                        break;
                }
            }

        });
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }
}