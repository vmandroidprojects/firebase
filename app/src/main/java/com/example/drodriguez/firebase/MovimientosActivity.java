package com.example.drodriguez.firebase;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class MovimientosActivity extends AppCompatActivity {
    private TabHost mTabHost;

    private EditText total;
    private EditText descripcion;


    private ListView movimientosList;
    private ListView bolsaList;

    private ArrayList<String> movimientosArray = new ArrayList<>();
    private ArrayList<String> bolsaArray = new ArrayList<>();

    private ArrayList<String> movimientoId = new ArrayList<>();
    private ArrayList<String> bolsaId = new ArrayList<>();

    FirebaseDatabase database = FirebaseDatabase.getInstance();;
    DatabaseReference myRef = database.getReference("tpvVM").child("usuarios").child(FirebaseAuth.getInstance().getCurrentUser().getUid());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movimientos);

        total = (EditText) findViewById(R.id.total);
        descripcion = (EditText) findViewById(R.id.fecha);

        movimientosList = (ListView) findViewById(R.id.movimientosList);
        bolsaList = (ListView) findViewById(R.id.movimientosBolsaList);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,movimientosArray);
        ArrayAdapter<String> arrayAdapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,bolsaArray);

        bolsaList.setAdapter(arrayAdapter2);
        movimientosList.setAdapter(arrayAdapter);


        myRef.child("movimientos").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                String desc = dataSnapshot.child("descripcion").getValue(String.class);
                String key = dataSnapshot.getKey().toString();
                if(dataSnapshot.child("tipoVenta").getValue(String.class).equals("Efectivo")){
                    movimientosArray.add(desc);
                    movimientoId.add(key);
                } else {
                    bolsaArray.add(desc);
                    bolsaId.add(key);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        movimientosList.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startActivity(new Intent(MovimientosActivity.this,DetalleMovimiento.class).putExtra("movimientoId",movimientoId.get(position)));
            }

        });

        bolsaList.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startActivity(new Intent(MovimientosActivity.this,DetalleMovimiento.class).putExtra("movimientoId",bolsaId.get(position)));
            }

        });

        mTabHost = (TabHost) findViewById(R.id.tabhost);
        mTabHost.setup();
        TabHost.TabSpec specs = mTabHost.newTabSpec("nuevoMovimiento");
        specs.setContent(R.id.nuevoMovimiento);
        specs.setIndicator("BOLSA");
        mTabHost.addTab(specs);

        specs = mTabHost.newTabSpec("movimientos");
        specs.setContent(R.id.movimientos);
        specs.setIndicator("CAJA");
        mTabHost.addTab(specs);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(MovimientosActivity.this,AccountActivity.class));
    }

}