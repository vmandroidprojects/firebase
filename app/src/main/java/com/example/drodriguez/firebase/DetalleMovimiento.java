package com.example.drodriguez.firebase;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class DetalleMovimiento extends AppCompatActivity {

    private TextView descripcion;
    private TextView fecha;
    private TextView accion;
    private TextView monto;

    private Button verVenta;

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("tpvVM").child("usuarios").child(FirebaseAuth.getInstance().getCurrentUser().getUid());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_movimiento);

        descripcion = (TextView) findViewById(R.id.descripcion);
        fecha = (TextView) findViewById(R.id.fecha);
        accion = (TextView) findViewById(R.id.accion);
        monto = (TextView) findViewById(R.id.monto);

        verVenta = (Button) findViewById(R.id.verVenta);

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                descripcion.setText(dataSnapshot.child("movimientos").child(getIntent().getStringExtra("movimientoId")).child("descripcion").getValue(String.class));
                fecha.setText(dataSnapshot.child("movimientos").child(getIntent().getStringExtra("movimientoId")).child("fecha").getValue(String.class));
                accion.setText(dataSnapshot.child("movimientos").child(getIntent().getStringExtra("movimientoId")).child("accion").getValue(String.class));
                monto.setText("$"+dataSnapshot.child("ventas").child(getIntent().getStringExtra("movimientoId")).child("total").getValue(String.class));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        verVenta.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                startActivity(new Intent(DetalleMovimiento.this,DetalleVenta.class).putExtra("ventaId",getIntent().getStringExtra("movimientoId")));
            }
        });
    }
}