package com.example.drodriguez.firebase;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewDebug;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class VentasActivity extends AppCompatActivity {
    private TabHost mTabHost;

    private EditText total;
    private EditText descripcion;

    private CheckBox pagoTarjeta;

    private Button cerrarVenta;

    private ListView ventasList;

    private DateFormat df;

    private String caja;
    private String bolsa;

    private ArrayList<String> ventasArray = new ArrayList<>();

    private ArrayList<String> ventaId = new ArrayList<>();

    FirebaseDatabase database = FirebaseDatabase.getInstance();;
    DatabaseReference myRef = database.getReference("tpvVM").child("usuarios").child(FirebaseAuth.getInstance().getCurrentUser().getUid());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ventas);

        total = (EditText) findViewById(R.id.total);
        descripcion = (EditText) findViewById(R.id.fecha);

        pagoTarjeta = (CheckBox) findViewById(R.id.pagoTarjeta);

        cerrarVenta = (Button) findViewById(R.id.cerrarVenta);

        ventasList = (ListView) findViewById(R.id.ventasList);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,ventasArray);

        ventasList.setAdapter(arrayAdapter);

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                caja = dataSnapshot.child("caja").getValue(String.class);
                bolsa = dataSnapshot.child("bolsa").getValue(String.class);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        myRef.child("ventas").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                ventasArray.add(dataSnapshot.child("descripcion").getValue(String.class)+"-$"+dataSnapshot.child("total").getValue(String.class));
                ventaId.add(dataSnapshot.getKey().toString());
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        ventasList.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startActivity(new Intent(VentasActivity.this,DetalleVenta.class).putExtra("ventaId",ventaId.get(position)));
            }

        });

        //Acción al presionar el boton de Cerrar Venta.
        cerrarVenta.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                vender(total.getText().toString(),descripcion.getText().toString(),myRef);
            }
        });

        mTabHost = (TabHost) findViewById(R.id.tabhost);
        mTabHost.setup();
        TabHost.TabSpec specs = mTabHost.newTabSpec("nuevaVenta");
        specs.setContent(R.id.nuevaVenta);
        specs.setIndicator("Nueva Venta");
        mTabHost.addTab(specs);

        specs = mTabHost.newTabSpec("ventas");
        specs.setContent(R.id.ventas);
        specs.setIndicator("VENTAS");
        mTabHost.addTab(specs);
    }

    //Verificar datos de registro.
    private void vender(final String total,final String descripcion,final DatabaseReference myRef) {
        if (TextUtils.equals(total,"0")) {
            Toast.makeText(VentasActivity.this, "INSERTA EL TOTAL", Toast.LENGTH_LONG).show();
        }else if (TextUtils.isEmpty(descripcion)) {
            Toast.makeText(VentasActivity.this, "INSERTA DESCRIPCION", Toast.LENGTH_LONG).show();
        } else {
            df = new SimpleDateFormat("d MMM yyyy, HH:mm");
            //Registro de Venta
            DatabaseReference ventas = myRef.child("ventas").push();
            DatabaseReference movimientos = myRef.child("movimientos").child(ventas.getKey());
            ventas.child("fecha").setValue(df.format(Calendar.getInstance().getTime()));
            ventas.child("total").setValue(total);
            ventas.child("descripcion").setValue(descripcion);

            movimientos.child("accion").setValue("ENTRADA");
            movimientos.child("fecha").setValue(df.format(Calendar.getInstance().getTime()));
            movimientos.child("descripcion").setValue("VENTA");
            if(!pagoTarjeta.isChecked()) {
                ventas.child("tipoVenta").setValue("Efectivo");
                movimientos.child("tipoVenta").setValue("Efectivo");
                myRef.child("caja").setValue(Double.toString((Double.parseDouble(total)+Double.parseDouble(caja))));
            } else{
                ventas.child("tipoVenta").setValue("Tarjeta");
                movimientos.child("tipoVenta").setValue("Tarjeta");
                myRef.child("bolsa").setValue(Double.toString((Double.parseDouble(total)+Double.parseDouble(bolsa))));
            }
            Toast.makeText(VentasActivity.this, "VENTA REALIZADA", Toast.LENGTH_LONG).show();
            startActivity(new Intent(VentasActivity.this,VentasActivity.class));
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(VentasActivity.this,AccountActivity.class));
    }
}