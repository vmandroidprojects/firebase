package com.example.drodriguez.firebase;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class DetalleVenta extends AppCompatActivity {

    private TextView descripcion;
    private TextView fecha;
    private TextView tipoVenta;
    private TextView total;



    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("tpvVM").child("usuarios").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("ventas");
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_venta);

        descripcion = (TextView) findViewById(R.id.descripcion);
        fecha = (TextView) findViewById(R.id.fecha);
        tipoVenta = (TextView) findViewById(R.id.tipoVenta);
        total = (TextView) findViewById(R.id.total);

        myRef.child(getIntent().getStringExtra("ventaId")).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                descripcion.setText(dataSnapshot.child("descripcion").getValue(String.class));
                fecha.setText(dataSnapshot.child("fecha").getValue(String.class));
                tipoVenta.setText(dataSnapshot.child("tipoVenta").getValue(String.class));
                total.setText("$"+dataSnapshot.child("total").getValue(String.class));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
