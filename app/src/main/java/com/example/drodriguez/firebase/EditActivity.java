package com.example.drodriguez.firebase;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.server.converter.StringToIntConverter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class EditActivity extends AppCompatActivity {
    private EditText name;
    private EditText lastname;
    private EditText status;
    private EditText bolsa;

    private Button saveButton;

    FirebaseDatabase database = FirebaseDatabase.getInstance();;
    DatabaseReference myRef = database.getReference("tpvVM").child("usuarios").child(FirebaseAuth.getInstance().getCurrentUser().getUid());
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        name = (EditText) findViewById(R.id.nombre);
        lastname = (EditText) findViewById(R.id.apellido);
        status = (EditText) findViewById(R.id.estatus);
        bolsa = (EditText) findViewById(R.id.bolsa);

        saveButton = (Button) findViewById(R.id.saveButton);

        saveButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                saveChanges(name.getText().toString(),lastname.getText().toString(),status.getText().toString(),bolsa.getText().toString(),myRef);
            }
        });
    }

    private void saveChanges(final String name,final String lastname, final String status, final String bolsa, final DatabaseReference myRef) {
        if (!TextUtils.isEmpty(name))
            myRef.child("nombre").setValue(name);
        if (!TextUtils.isEmpty(lastname))
            myRef.child("apellido").setValue(lastname);
        if (!TextUtils.isEmpty(bolsa))
            myRef.child("bolsa").setValue(bolsa);
        if (!TextUtils.isEmpty(status))
            myRef.child("estatus").setValue(status);
        Toast.makeText(EditActivity.this, "CAMBIOS GUARDADOS", Toast.LENGTH_LONG).show();
    }
}
